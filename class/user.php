<?php

class User{	
	
	function hasJob($uid){
		global $db;
		$job = $db->get_row("SELECT * FROM jobs WHERE user_id = {$uid}");
		if(is_object($job)){
			$in = new DateTime($job->check_in);
			$out = new DateTime($job->check_out);
			
			$job->check_in = $in->format('m/d/Y');
			$job->check_out = $out->format('m/d/Y');
			
			return $job;
		}else{
			return false;
		}
	}
	
	function createJob($checkin, $checkout, $uid, $update=0){
		global $db;
		if($update > 0){
			if($db->query("UPDATE jobs SET check_in = '{$checkin}', check_out = '{$checkout}' WHERE id = {$update} AND user_id = {$uid}")){
				return true;
			}else{ return $db->debug();exit; }
		}else{
			if($db->query("INSERT INTO jobs(user_id, check_in, check_out) VALUES ({$uid}, '{$checkin}', '{$checkout}')")){
				return true;
			}else{ return $db->debug();exit; }
		}
		
		$_SESSION['job_created'] = "Check in and check out dates has been set successfully.";
		header("Location: index.php");
		exit;
		
	}
	
	function removeJob($uid, $id){
		global $db;
		
		if($db->query("DELETE FROM jobs WHERE id = {$id} AND user_id = {$uid}")){
			return true;
		}else{ return $db->debug(); }
	}
	
}

$user = new User;