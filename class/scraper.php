<?php

class Scraper{
	
	public $cityHotel, $spiderHotel, $json, $post, $cookie;
	public $timeout = 5;
	
	function getContent($cookie=0){
	
		$url = str_replace( "&amp;", "&", urldecode(trim($this->cityHotel)) );
		$this->cookie = tempnam ("/tmp", "CURLCOOKIE");
		
		$ch = curl_init();
		
		curl_setopt( $ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; rv:1.7.3) Gecko/20041001 Firefox/0.10.1" );
		curl_setopt( $ch, CURLOPT_URL, $url );
		
		if(!empty($this->post)){
			curl_setopt($ch,CURLOPT_POST, TRUE);
			curl_setopt($ch,CURLOPT_POSTFIELDS, $this->post);
		}
		
		curl_setopt($ch, CURLOPT_COOKIESESSION, true);
		curl_setopt( $ch, CURLOPT_COOKIEJAR, $this->cookie );
		curl_setopt( $ch, CURLOPT_COOKIEFILE, $this->cookie );
		
		curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, true );
		curl_setopt( $ch, CURLOPT_ENCODING, "" );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch, CURLOPT_AUTOREFERER, true );
		curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT, $this->timeout );
		curl_setopt( $ch, CURLOPT_TIMEOUT, $this->timeout );
		curl_setopt( $ch, CURLOPT_MAXREDIRS, 10 );
		
		curl_exec( $ch );
		
		curl_setopt($ch, CURLOPT_URL, $this->spiderHotel);
		curl_setopt($ch, CURLOPT_POST, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "");
		curl_exec($ch);
		
		curl_setopt($ch, CURLOPT_URL, $this->json);
		curl_setopt($ch, CURLOPT_POST, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "");
		$content = curl_exec($ch);
		
		return $content;
	}
	
}

$scraper = new Scraper;