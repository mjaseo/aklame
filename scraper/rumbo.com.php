<?php 

class Scraper{
	
	public $url, $post, $cookie;
	public $timeout = 5;
	
	function getContent($cookie=0){
	
		$url = str_replace( "&amp;", "&", urldecode(trim($this->url)) );
		$this->cookie = tempnam ("/tmp", "CURLCOOKIE");
		
		$ch = curl_init();
		
		curl_setopt( $ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; rv:1.7.3) Gecko/20041001 Firefox/0.10.1" );
		curl_setopt( $ch, CURLOPT_URL, $url );
		
		if(!empty($this->post)){
			curl_setopt($ch,CURLOPT_POST, TRUE);
			curl_setopt($ch,CURLOPT_POSTFIELDS, $this->post);
		}
		
		curl_setopt($ch, CURLOPT_COOKIESESSION, true);
		curl_setopt( $ch, CURLOPT_COOKIEJAR, $this->cookie );
		curl_setopt( $ch, CURLOPT_COOKIEFILE, $this->cookie );
		
		curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, true );
		curl_setopt( $ch, CURLOPT_ENCODING, "" );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch, CURLOPT_AUTOREFERER, true );
		curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT, $this->timeout );
		curl_setopt( $ch, CURLOPT_TIMEOUT, $this->timeout );
		curl_setopt( $ch, CURLOPT_MAXREDIRS, 10 );
		
		curl_exec( $ch );
		
		curl_setopt($ch, CURLOPT_URL, 'http://www.rumbo.com/alojamientos/hoteles/spiderHotel.do');
		curl_setopt($ch, CURLOPT_POST, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "");
		curl_exec($ch);
		
		curl_setopt($ch, CURLOPT_URL, 'http://www.rumbo.com/alojamientos/availNewJSON.ajax');
		curl_setopt($ch, CURLOPT_POST, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "");
		$content = curl_exec($ch);
		
		return $content;
	}
	
}

if(isset($_POST['search'])){

	$start_date = urlencode($_POST['start_date']);
	$end_date = urlencode($_POST['end_date']);
	
	$scraper = new Scraper;
	
	$scraper->post = 'searchRestricted=&Nhotel=no&from=MAIN-HOME&provinciaSP=&localidadSP=Selecciona+una+ciudad&paisIN=-----&localidadIn=&autocompleter-userInput-arrCity=Pacifica+--+California+%28United+States%29&provSelec=California&arrCountry=United+States&depDate='.$start_date.'&retDate='.$end_date.'&arrCity=Pacifica&paxHab=2A0N&numRooms=1&arrHotel=&boardType=0&category=0&nights=1&child1=&child2=&child3=&child4=&child5=&child6=&child7=&child8=&child9=&child10=&child11=&child12=&child13=&child14=&child15=&paxAdt=2&paxChd=0&quickSearch=Y';
	
	$scraper->url = 'http://www.rumbo.com/alojamientos/hoteles/cityHotel.do';
	
	$result = json_decode($scraper->getContent());
	$num = 0;
	
	if(!empty($result)){
		$node = $result->AvailResponse;
		
		if($node){
			$num = $node->Pagina->numHotelAvailability;
			$hotels = $node->Hotels;
		}
	}
}

?>

<!DOCTYPE html>
<html>
    <head>
        <title>Rumbo.com - Hotel Search</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Bootstrap CSS and bootstrap datepicker CSS used for styling the demo pages-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    </head>
    <body>
        <div class="container">
            <div class="hero-unit"><br />
            	<form method="post">
	                Start Date : <input  type="text" placeholder="Start Date" id="start_date" name="start_date" value="<?php echo @$_POST['start_date']; ?>">
	                End Date : <input  type="text" placeholder="End Date" id="end_date" name="end_date" value="<?php echo @$_POST['end_date']; ?>">
	                <button type="submit" class="btn btn-primary" name="search">Search</button>
            	</form>
            </div>
            <br />
            <table class="table table-bordered"> 
            <?php if(isset($num) && $num <= 0){ ?>
            	<thead> 
            		<tr> 
            			<th colspan="4">We did not find available hotels for the selected dates. Please modify your search and try it again.</th> 
            		</tr> 
            	</thead> 
            <?php }else{ 
            	
            	if(!empty($hotels)):
            	
            	foreach(array_slice($hotels, 0, 5) as $hotel): ?>
            	
            	<thead> 
            		<tr> 
            			<th><h3 class="text-primary"><?php echo $hotel->Name; ?> - <?php echo $hotel->LocationInfo->Address; ?></h3></th> 
            			<th><h4 class="text-center text-primary">Room/Night</h4></th>
            		</tr> 
            	</thead> 
            	<tbody> 
            	<?php foreach($hotel->Boards[0]->RoomCombinations as $rooms): ?>
            		<tr> 
            			<th scope="row"><h4 class="text-justify"><?php echo $rooms->Rooms[0]->Description; ?> (<?php echo $rooms->Rooms[0]->BoardDescription; ?>)</h4> </th> 
            			<td><h3 class="text-center"><?php echo $rooms->PricePerRoomPerNight; ?>€</h3></td>
            		</tr> 
            	<?php endforeach; ?>
            	</tbody> 
            <?php 
            	endforeach; endif; 
            } 
            ?>
            </table>
        </div>
        <!-- Load jQuery and bootstrap datepicker scripts -->
        <script src="https://code.jquery.com/jquery-1.12.1.min.js" integrity="sha256-I1nTg78tSrZev3kjvfdM5A5Ak/blglGzlaZANLPDl3I=" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.js"></script>
        <script type="text/javascript">
            // When the document is ready
            $(document).ready(function () {
                
                $('#start_date').datepicker({
                    format: "dd/mm/yyyy"
                }); 
                
                $('#end_date').datepicker({
                    format: "dd/mm/yyyy"
                }); 
            
            });
        </script>
    </body>
</html>