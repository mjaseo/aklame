<?php
require 'ezSQL/db.php';
require 'class/user.php';
if(!isset($_SESSION['isLoggedIn'])) header("Location: login.php");

$user_id = @$_SESSION['isLoggedIn']['id'];
$hasJob = $user->hasJob($user_id);

include_once('template/header.php'); ?>


        <div class="content">
            <div class="container-fluid">
            	<div class="row">
            		<div class="col-md-8">
		            	<div class="card">
		            		<div class="header">
			            		<?php if(!$hasJob){ ?>
			            			<?php if(isset($_SESSION['job_deleted'])): ?>
			            				<div class="alert alert-info">
										<span><b> Settings Deleted! <?php echo $_SESSION['job_deleted']; ?></span>
                                		</div>
			            			<?php unset($_SESSION['job_deleted']); endif; ?>
		                        <h4 class="title">Create a Job</h4>
		                        <p class="category">You need to <a href="setup.php">setup</a> a check-in and check-out dates to start a job.</p>
		                    <?php }else{ ?>
		                    		<?php if(isset($_SESSION['job_created'])){ ?>
		                    			<div class="alert alert-info">
										<span><b> Thank You! </b> <?php echo $_SESSION['job_created']; ?></span>
                                		</div>
		                    		<?php unset($_SESSION['job_created']);
			                    	 }else{ ?>
		                    			<h5 class="title">Current job settings:</h5>
		                    		<?php } ?>
		                        <?php   
				                		$checkin = strtotime($hasJob->check_in);
				                    $checkout = strtotime($hasJob->check_out);
			                    ?>
			                    <h3>Check In : <?php echo date('jS F Y', $checkin); ?></h3>
			                    <h3>Check Out : <?php echo date('jS F Y', $checkout); ?></h3>
		                    <?php } ?>
		                    <?php if($hasJob){ ?>
		                    		<br />
		                    		<button class="btn btn-info btn-fill pull-left" onClick="window.location='setup.php'">Update Settings</button><div style="float:left">&nbsp;</div>
		                    		<button class="btn btn-danger btn-fill pull-left" id="removeJob">Remove Settings</button>
		                    	<?php } ?>
		                    </div>
		                    <div class="content">
		                    	<div class="clearfix"></div>
		                    </div>
		            	</div>
            		</div>
            	</div>
            </div>
        </div>


<?php include_once('template/footer.php'); ?>

</html>