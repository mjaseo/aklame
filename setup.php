<?php
require 'ezSQL/db.php';
require 'class/user.php';
if(!isset($_SESSION['isLoggedIn'])) header("Location: login.php");

$userid = $_SESSION['isLoggedIn']['id'];
$hasJob = $user->hasJob($userid);

if(isset($_GET['remove']) && $hasJob){
	if($user->removeJob($userid, $hasJob->id)){
		$_SESSION['job_deleted'] = "Check-in and checkout settings has been successfully removed.";
		header("Location: index.php");
		exit;
	}
}

if(isset($_POST) && !empty($_POST)){
	
	@$hasJob->check_in = $checkin = $_POST['checkin_date'];
	@$hasJob->check_out = $checkout = $_POST['checkout_date'];
	
	if(empty($checkin))
	{
		$hasError[] = "Check-in date is required.";
	}
	if(empty($checkout))
	{
		$hasError[] = "Check-out date is required.";
	}
	
	if(empty(@$hasError)){
		
		$checkin = new DateTime($_POST['checkin_date']);
		$checkout = new DateTime($_POST['checkout_date']);
		$update = (isset($_POST['update'])) ? $_POST['update'] : 0;
		
		$checkin = $db->escape($checkin->format('Y-m-d'));
		$checkout = $db->escape($checkout->format('Y-m-d'));
		if($user->createJob($checkin, $checkout, $userid, $update)){
			$_SESSION['job_created'] = "Check-in and checkout dates has been set successfully.";
			header("Location: index.php");
			exit;
		}
	}
}

$title = 'Setup';
include_once('template/header.php'); ?>


        <div class="content">
            <div class="container-fluid">
            	<div class="row">
            		<div class="col-md-8">
		            	<div class="card">
		            		<div class="header">
		                        <h4 class="title"><?php echo ($hasJob) ? 'Edit' : 'Add a'; ?> Job</h4>
		                    </div>
		                    <div class="content">
			               <?php if(@$hasError && is_array(@$hasError)): ?>
		                    	<div class="alert alert-danger">
                                <span><?php echo implode("<br />", $hasError); ?></span>
                            </div>
                            <?php endif; ?>
		                    <form method="post">
		                    	<div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Check-In Date</label>
                                                <div class="input-group">
										            <input type="text" placeholder="Pick a date" class="form-control docs-date" name="checkin_date" value="<?php echo @$hasJob->check_in; ?>">
										            <span class="input-group-btn">
										              <button class="btn btn-default docs-datepicker-trigger" type="button" disabled="">
										                <i aria-hidden="true" class="glyphicon glyphicon-calendar"></i>
										              </button>
										            </span>
										         </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Check-Out Date</label>
                                                <div class="input-group">
										            <input type="text" placeholder="Pick a date" class="form-control docs-date" name="checkout_date" value="<?php echo @$hasJob->check_out; ?>">
										            <span class="input-group-btn">
										              <button class="btn btn-default docs-datepicker-trigger" type="button" disabled="">
										                <i aria-hidden="true" class="glyphicon glyphicon-calendar"></i>
										              </button>
										            </span>
										         </div>
                                            </div>
                                        </div>
                                 </div>
                                 <?php if($hasJob){ ?>
                                 	<input type="hidden" value="<?php echo $hasJob->id; ?>" name="update">
                                 	<button class="btn btn-info btn-fill" type="submit">Update Job</button><div style="float:left">&nbsp;</div>
                                 	<button class="btn btn-danger btn-fill" id="removeJob">Remove Settings</button>
                                 <?php }else{ ?>
                                 	<button class="btn btn-info btn-fill pull-left" type="submit">Create Job</button>
                                 <?php } ?>
                                 </form>
                                 <div class="clearfix"></div>
		                    </div>
		            	</div>
            		</div>
            	</div>
            </div>
        </div>


<?php include_once('template/footer.php'); ?>

</html>