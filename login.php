<?php
require 'ezSQL/db.php';

if(isset($_POST) && !empty($_POST)){
	
	$username = $db->escape($_POST['username']);
	$password = $db->escape($_POST['password']);
	
	$user = $db->get_row("SELECT id,username FROM users WHERE username = '{$username}' AND password = '{$password}'");
	
	if(is_object($user)){
		$_SESSION['isLoggedIn'] = array(
			'id' => $user->id,
			'name' => $user->username
		);
		header("Location: index.php");
		exit;
	}else{
		$loggedInError = true;
	}
	
}

$title = 'Login';
include_once('template/header.php'); ?>


        <div class="content">
            <div class="container-fluid">
            	<div class="row">
            		<div class="col-md-8">
		            	<div class="card">
		            		<div class="header">
		                        <h4 class="title">Login</h4>
		                    </div>
		                    <div class="content">
		                    <?php if(@$loggedInError): ?>
		                    	<div class="alert alert-danger">
                                    <span><b> Login Failed! - </b> Invalid username or password. Please try again.</span>
                                </div>
                            <?php endif; ?>
		                    <form method="post">
		                    	<div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Username</label>
                                                <div class="form-group">
										            <input class="form-control" type="text" value="<?php echo @$_POST['username']; ?>" placeholder="Username" name="username">
										         </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Password</label>
                                                <div class="form-group">
										            <input class="form-control" type="password" value="<?php echo @$_POST['password']; ?>" placeholder="Password" name="password">
										         </div>
                                            </div>
                                        </div>
                                 </div>
                                 <button class="btn btn-info btn-fill pull-left" type="submit">Login</button>
                                 </form>
                                 <div class="clearfix"></div>
		                    </div>
		            	</div>
            		</div>
            	</div>
            </div>
        </div>


<?php include_once('template/footer.php'); ?>

</html>